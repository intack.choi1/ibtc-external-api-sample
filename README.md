# IBTC 기술 거래소 외부 API 
IBTC 기술 거래소 외부 API 호출을 위한 샘플
### 설정
```
git clone https://gitlab.com/intack.choi1/ibtc-external-api-sample
cd ibtc-external-api-sample && npm install
```

## 닉네임 중복 확인
```
SECRET_KEY=<비밀키> node nickname.js <중복을 확인할 닉네임>
# 예)
SECRET_KEY=<비밀키> node nickname.js 비트코인화이팅
```

## 회원가입
```
SECRET_KEY=<비밀키> node signup.js <아이디> <이름> <닉네임> <비밀번호>
# 예)
SECRET_KEY=<비밀키> node signup.js tester@naver.com 최인탁 비트코인화이팅 p@32sZcvc
```

## 이메일 발송
```
SECRET_KEY=<비밀키> node email.js <받을 이메일 주소> <제목> <내용>
# 예)
SECRET_KEY=<비밀키> node email.js timetorunn@naver.com title "$(cat email.html)"
```

## PDF 문서 업로드
```
node upload.js <PDF 문서 경로>

# 예)
node upload.js ./test.pdf
```

