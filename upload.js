var axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
var filePath = process.argv[2] || 'filePath'
var data = new FormData();
data.append('file', fs.createReadStream(filePath));

var config = {
  method: 'post',
  url: 'http://3.35.203.44/upload',
  headers: { 
    ...data.getHeaders()
  },
  data : data
};

axios(config)
.then(function (response) {
  console.log(JSON.stringify(response.data));
})
.catch(function (error) {
  console.log(error);
});
