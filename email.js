const SECRET_KEY = process.env.SECRET_KEY || 'secret-key'
const DOMAIN = 'http://3.35.203.44'
const { createHmac } = require('crypto')
const axios = require('axios')
const to = process.argv[2] || 'to'
const subject = process.argv[3] || 'subject'
const body = process.argv[4] || 'body'

const message = {
  to,
  subject,
  body,
  timestamp: new Date().getTime()
}
const messagePlainText = JSON.stringify(message)
const hmac = createHmac('sha256', SECRET_KEY)
hmac.update(messagePlainText)
const digest = hmac.digest('base64')

console.log('요청 payload:', messagePlainText)
console.log('메시지 다이제스트:', digest)

axios
  .post(`${DOMAIN}/email`, message, {
    headers: {
      Digest: `SHA-256=${digest}`,
    }
  })
  .then(res => {
    console.log('성공 응답:', res.data)
  })
  .catch(error => {
    if (error.response && error.response.data) 
      console.error('실패 응답:', error.response.data)
  })

